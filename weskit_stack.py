#  Copyright (c) 2021. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#      https://gitlab.com/one-touch-pipeline/weskit/api/-/blob/master/LICENSE
#
#  Authors: The WESkit Team

import argparse
import os
import sys
import subprocess
import time
from source.api_demo import validate_token, run_demo, get_user_token_from_keycloak

BASE_COMPOSE = "static/weskit_stack_base.yaml"

# 1. process arguments
parser = argparse.ArgumentParser(description="Deploy WESkit services on Docker Swarm.")

subparser = parser.add_subparsers(dest="mode")
start_parser = subparser.add_parser("start")
test_parser = subparser.add_parser("test")

start_parser.add_argument("--compose",
                          help="User compose file extending the base compose file",
                          type=str, required=False)

start_parser.add_argument("--login",
                          help="Activate login management in WESkit and start pre-configured\
                                keycloak server.",
                          action='store_true')

start_parser.add_argument("--minio",
                          help="Add Minio as S3 service.",
                          action='store_true')

test_parser.add_argument("--login",
                         help="Run tests using login.",
                         action='store_true')

args = parser.parse_args()

# hard coded config values
config = {
    "keycloak_weskit_token_endpoint": "http://localhost:8080/realms/weskit/protocol/openid-connect/token",
    "keycloak_user_credentials": {
        "grant_type": "password",
        "username": "test",
        "password": "test",
        "client_id": "weskit",
        "client_secret": "vzR9PtrYBLpOpJ110oz9trlZaJp5nNMP"},
    "weskit_host": "https://localhost",
    "cert": os.path.abspath("certs/weskit.crt")
}

# start new stack
if args.mode == "start":
    base_command = ["docker", "stack", "deploy"]
    compose_files = ["-c", BASE_COMPOSE]
    if args.compose:
        if os.path.isfile(args.compose):
            compose_files += ["-c", args.compose]
        else:
            print("Error: User compose file does not exist.")
            sys.exit()
    if args.login:
        compose_files += ["-c", "static/keycloak.yaml"]
    if args.minio:
        compose_files += ["-c", "static/minio.yaml"]
    try:
        subprocess.run(base_command + compose_files + ["weskit"])
    except Exception:
        print("ERROR: can not run the stack.")
        sys.exit()

# wait and additional configuration
if args.mode == "start" and args.login:
    print("Waiting for running stack (1 min)")
    time.sleep(60)

    print("Create test user")
    validate_token(
        client_data=config["keycloak_user_credentials"],
        client_token_endpoint=config["keycloak_weskit_token_endpoint"],
        cert=config["cert"])

if args.mode == "test":
    header = None
    if args.login:
        try:
            print("Run test with user login")
            token = get_user_token_from_keycloak(config["keycloak_weskit_token_endpoint"],
                                                 config["keycloak_user_credentials"],
                                                 config["cert"])
            header = dict(Authorization="Bearer " + token["access_token"])
        except Exception:
            print("ERROR: can not reach keycloak. Is keycloak running?")
            print("       Continue without login.")
    try:
        run_demo(config["weskit_host"], config["cert"], header)
    except Exception as e:
        print(f"ERROR: can not reach weskit. {e}")
